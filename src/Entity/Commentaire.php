<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Entity\Commentaire;

/**
* @ORM\Entity(repositoryClass="App\Repository\CommentaireRepository")
*/
class Commentaire
{
  /**
  * id de Commentaire
  * @var int
  * @ORM\Id()
  * @ORM\GeneratedValue()
  * @ORM\Column(type="integer")
  */
  private $id;

  /**
  * dateCreation de Commentaire
  * @var datetime
  * @ORM\Column(type="datetime")
  */
  private $dateCreation;

  /**
  * texte de Commentaire
  * @var string
  * @ORM\Column(type="text", nullable=true)
  */
  private $texte;

  /**
  * user de Commentaire
  * @var User
  * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="commentaires")
  * @ORM\JoinColumn(nullable=false)
  */
  private $user;


  /**
  * post de Commentaire
  * @var Post
  * @ORM\ManyToOne(targetEntity="App\Entity\Post", inversedBy="commentaire")
  */
  private $post;

  /**
  * Récupère l'id de Commentaire
  * @return int
  */
  public function getId(): ?int
  {
    return $this->id;
  }

  /**
  * Récupère la dateCreation de Commentaire
  * @return datetime
  */
  public function getDateCreation(): ?\DateTimeInterface
  {
    return $this->dateCreation;
  }

  /**
  * Set la dateCreation de Commentaire
  * @param  DateTimeInterface $dateCreation
  * @return self
  */
  public function setDateCreation(\DateTimeInterface $dateCreation): self
  {
    $this->dateCreation = $dateCreation;

    return $this;
  }

  /**
  * Récupère le texte de Commentaire
  * @return string
  */
  public function getTexte(): ?string
  {
    return $this->texte;
  }

  /**
  * Défini le texte de Commentaire
  * @param  ?string $texte
  * @return self
  */
  public function setTexte(?string $texte): self
  {
    $this->texte = $texte;

    return $this;
  }

  /**
  * Récupère l'user de Commentaire
  * @return User
  */
  public function getUser(): ?User
  {
    return $this->user;
  }

  /**
  * Set l'user de Commentaire
  * @param  ?User $user
  * @return self
  */
  public function setUser(?User $user): self
  {
    $this->user = $user;

    return $this;
  }

  /**
  * Récupère le Post de Commentaire
  * @return Post
  */
  public function getPost(): ?Post
  {
    return $this->post;
  }

  /**
  * Set le post de Commentaire
  * @param  ?Post $post
  * @return self
  */
  public function setPost(?Post $post): self
  {
    $this->post = $post;

    return $this;
  }
}
