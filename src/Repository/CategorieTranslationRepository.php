<?php

namespace App\Repository;

use App\Entity\CategorieTranslation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
* @method CategorieTranslation|null find($id, $lockMode = null, $lockVersion = null)
* @method CategorieTranslation|null findOneBy(array $criteria, array $orderBy = null)
* @method CategorieTranslation[]    findAll()
* @method CategorieTranslation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
*/
class CategorieTranslationRepository extends ServiceEntityRepository
{
  public function __construct(RegistryInterface $registry)
  {
    parent::__construct($registry, CategorieTranslation::class);
  }

  // /**
  //  * @return CategorieTranslation[] Returns an array of CategorieTranslation objects
  //  */
  /*
  public function findByExampleField($value)
  {
  return $this->createQueryBuilder('c')
  ->andWhere('c.exampleField = :val')
  ->setParameter('val', $value)
  ->orderBy('c.id', 'ASC')
  ->setMaxResults(10)
  ->getQuery()
  ->getResult()
  ;
}
*/

/*
public function findOneBySomeField($value): ?CategorieTranslation
{
return $this->createQueryBuilder('c')
->andWhere('c.exampleField = :val')
->setParameter('val', $value)
->getQuery()
->getOneOrNullResult()
;
}
*/
}
