<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use App\Entity\CategorieTranslation;
use Cocur\Slugify\Slugify;


/**
* @ORM\Entity(repositoryClass="App\Repository\CategorieTranslationRepository")
*/
class CategorieTranslation
{

  use ORMBehaviors\Translatable\Translation;



  public function __call($method, $arguments)
  {
    $method = ('get' === substr($method, 0, 3) || 'set' === substr($method, 0, 3)) ? $method : 'get'. ucfirst($method);

    return $this->proxyCurrentLocaleTranslation($method, $arguments);
  }

  public function __get($name)
  {
    $method = 'get'. ucfirst($name);
    $arguments = [];
    return $this->proxyCurrentLocaleTranslation($method, $arguments);
  }




  /**
  * nom de Categorie(Translation)
  * @var string
  * @ORM\Column(type="string", length=255)
  */

  private $nom;

  /**
  * description de Categorie(Translation)
  * @var string
  * @ORM\Column(type="text", nullable=true)
  */

  private $description;

  /**
  * Récupère l'id de Categorie(Translation)
  * @return [type] [description]
  */
  public function getId(): ?int
  {
    return $this->id;
  }
  /**
  * Récupère le nom de Categorie(Translation)
  * @return [type] [description]
  */
  public function getNom(): ?string
  {
    return $this->nom;
  }

  /**
  * Function qui permet de slugify le titre de Page
  * @return string
  */
  public function getSlug(){
    $slugify = new Slugify();
    return $slugify->slugify($this->nom);
  }
  /**
  * Set le nom de Categorie(Translation)
  * @param  string $nom
  * @return self
  */
  public function setNom(string $nom): self
  {
    $this->nom = $nom;

    return $this;
  }
  /**
  * Récupère la description de Categorie(Translation)
  * @return [type] [description]
  */
  public function getDescription(): ?string
  {
    return $this->description;
  }
  /**
  * Set la description de Categorie(Translation)
  * @param  ?string $description
  * @return self
  */
  public function setDescription(?string $description): self
  {
    $this->description = $description;

    return $this;
  }



}
