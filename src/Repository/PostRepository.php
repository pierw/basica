<?php

namespace App\Repository;

use App\Entity\Post;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;


/**
* @method Post|null find($id, $lockMode = null, $lockVersion = null)
* @method Post|null findOneBy(array $criteria, array $orderBy = null)
* @method Post[]    findAll()
* @method Post[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
*/
class PostRepository extends ServiceEntityRepository
{
  public function __construct(RegistryInterface $registry)
  {
    parent::__construct($registry, Post::class);
  }

  // /**
  //  * @return Post[] Returns an array of Post objects
  //  */
  /*
  public function findByExampleField($value)
  {
  return $this->createQueryBuilder('p')
  ->andWhere('p.exampleField = :val')
  ->setParameter('val', $value)
  ->orderBy('p.id', 'ASC')
  ->setMaxResults(10)
  ->getQuery()
  ->getResult()
  ;
}
*/

/*
public function findOneBySomeField($value): ?Post
{
return $this->createQueryBuilder('p')
->andWhere('p.exampleField = :val')
->setParameter('val', $value)
->getQuery()
->getOneOrNullResult()
;
}
*/

/**
 * Function qui cherche les post ayant la categorie X
 * @param  int $id id de la categorie
 * @return Post  
 */
public function findCategorieId($id)
{
  return $this->createQueryBuilder('p')
  ->join('p.categorie','c')
  ->andWhere('c.id = :id')
  ->setParameter('id', $id)
  ->orderBy('p.dateCreation', 'desc')
  ->getQuery()
  ->getResult()
  ;
}






}
