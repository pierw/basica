<?php
/*
./src/Controller/PostController.php

* Controller des Post
*
* Action disponible show
*
* @author Original Author Pierre Wasilewski
* @copyright 1997-2005 The PHP Group
* @version 1.0.1
*/

namespace App\Controller;
use Ieps\Core\GenericController;
use App\Entity\Post;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Knp\Component\Pager\PaginatorInterface;


/**
 * Controller des Post
 */

class PostController extends GenericController {


      /**
       * [paginationAction description]
       * @param  Request            $request
       * @param  string             $vue
       * @param  PaginatorInterface $paginator
       * @param  array              $orderBy
       * @param  int                $limit
       * @param  int                $post
       * @param  int                $categorie
       * @return Response
       */
      public function paginationAction(Request $request,string $vue,PaginatorInterface $paginator , array $orderBy = ['dateCreation' => 'DESC'] ,int $limit = null,int $post= null,int $categorie = null){
            $where =($post !== null)?['post'=>$post]:[];
            $request = Request::createFromGlobals();
            if($categorie !== null) {
            $paginate = $this->_repository->findCategorieId($categorie);
            }
            else {
            $paginate = $this->_repository->findBy($where, $orderBy, $limit);
            }
            $posts= $paginator->paginate(
            $paginate, /* query NOT result */
            $request->query->getInt('page',1), /*page number*/
            $request->query->getInt('limit',4)
    );
            return $this->render('post/'.$vue.'.html.twig',[
            'posts' => $posts
          ]);
        }



}
