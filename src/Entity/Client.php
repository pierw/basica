<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
* @ORM\Entity(repositoryClass="App\Repository\ClientRepository")
*/
class Client {


  /**
  * function qui permet l'affichage des relation n m  dans l'easy admin
  * @return string [description]
  */
  public function __toString() {
    return $this->pseudo;
  }

  /**
  * l'id de Client
  * @var int
  * @ORM\Id()
  * @ORM\GeneratedValue()
  * @ORM\Column(type="integer")
  */
  private $id;


  /**
  * pseudo de Client
  * @var string
  * @ORM\Column(type="string", length=255)
  */
  private $pseudo;


  /**
  * works de Client
  * @var Work
  * @ORM\OneToMany(targetEntity="App\Entity\Work", mappedBy="client")
  */
  private $works;

  /**
  * Constructeur de Client
  */
  public function __construct()
  {
    $this->works = new ArrayCollection();
  }

  /**
  * Récupère l'id de Client
  * @return int
  */
  public function getId(): ?int
  {
    return $this->id;
  }

  /**
  * Récupère le pseudo de Client
  * @return string
  */
  public function getPseudo(): ?string
  {
    return $this->pseudo;
  }

  /**
  * Set le pseudo de Client
  * @param  string $pseudo
  * @return self
  */
  public function setPseudo(string $pseudo): self
  {
    $this->pseudo = $pseudo;

    return $this;
  }

  /**
  * @return Collection|Work[]
  */
  /**
  * Récupère les works de Client
  * @return Collection [description]
  */
  public function getWorks(): Collection
  {
    return $this->works;
  }
  /**
  * Ajoute des works à Client
  * @param  Work $work
  * @return self
  */
  public function addWork(Work $work): self
  {
    if (!$this->works->contains($work)) {
      $this->works[] = $work;
      $work->setClient($this);
    }

    return $this;
  }

  /**
  * Delete des works à Client
  * @param  Work $work
  * @return self
  */
  public function removeWork(Work $work): self
  {
    if ($this->works->contains($work)) {
      $this->works->removeElement($work);
      // set the owning side to null (unless already changed)
      if ($work->getClient() === $this) {
        $work->setClient(null);
      }
    }

    return $this;
  }
}
