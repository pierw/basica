<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Cocur\Slugify\Slugify;


/**
 * @ORM\Entity(repositoryClass="App\Repository\TagRepository")
 */
class Tag
{


    /**
     * id de Post
     * @var int
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;


    /**
     * nom de Post
     * @var string
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * description de Post
     * @var string
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * works de Tag
     * @var Work
     * @ORM\ManyToMany(targetEntity="App\Entity\Work", mappedBy="tag")
     */
    private $works;

    /**
     * Constructor de Tag
     */
    public function __construct()
    {
        $this->works = new ArrayCollection();
    }

    /**
     * function qui permet l'affichage des liaisons n m dans easy admin
     * @return string
     */
    public function __toString() {
    return $this->nom;
}

    /**
     * Récupère l'id de Tag
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Récupère le nom de Tag
     * @return string
     */
    public function getNom(): ?string
    {
        return $this->nom;
    }

    /**
    * Function qui permet de slugify le titre de Page
    * @return string
    */
    public function getSlug(){
      $slugify = new Slugify();
      return $slugify->slugify($this->nom);
    }
    /**
     * Set le nom de Tag
     * @param  string $nom
     * @return self
     */
    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Récupère la description de Tag
     * @return string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * Set la description de Tag
     * @param  string $description
     * @return self
     */
    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection|Work[]
     */
    /**
     * Récupère les works de Tag
     * @return Collection
     */
    public function getWorks(): Collection
    {
        return $this->works;
    }

    /**
     * Ajoute des work à Tag
     * @param  Work $work
     * @return self
     */
    public function addWork(Work $work): self
    {
        if (!$this->works->contains($work)) {
            $this->works[] = $work;
            $work->addTag($this);
        }

        return $this;
    }

    /**
     * Delete des work de Tag
     * @param  Work $work [description]
     * @return self       [description]
     */
    public function removeWork(Work $work): self
    {
        if ($this->works->contains($work)) {
            $this->works->removeElement($work);
            $work->removeTag($this);
        }

        return $this;
    }
}
