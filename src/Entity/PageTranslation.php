<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Cocur\Slugify\Slugify;

/**
* @ORM\Entity(repositoryClass="App\Repository\PageTranslationRepository")
*/
class PageTranslation
{   use ORMBehaviors\Translatable\Translation;


  /**
  * titre de Page(Translations)
  * @var string
  * @ORM\Column(type="string", length=255)
  */
  protected $titre;

  /**
  * texte de Page
  * @var string
  * @ORM\Column(type="text", nullable=true)
  */
  protected $text;

  /**
  * titreMenu de Page
  * @var string
  * @ORM\Column(type="string", length=255, nullable=true)
  */
  protected $titreMenu;


  /**
  * Récupère le titre de Page
  * @return string
  */
  public function getTitre(): ?string
  {
    return $this->titre;
  }

  /**
  * Function qui permet de slugify le titre de Page
  * @return string
  */
  public function getSlug(){
    $slugify = new Slugify();
    return $slugify->slugify($this->titre);
  }

  /**
  * Set titre de Page
  * @param  string $titre
  * @return self
  */
  public function setTitre(string $titre): self
  {
    $this->titre = $titre;

    return $this;
  }

  /**
  * Récupère le text de Page
  * @return string
  */
  public function getText(): ?string
  {
    return $this->text;
  }

  /**
  * Set le text de Page
  * @param  string $text
  * @return self
  */
  public function setText(string $text): self
  {
    $this->text = $text;

    return $this;
  }

  /**
  * Récupère le titreMenu de Page
  * @return string
  */
  public function getTitreMenu(): ?string
  {
    return $this->titreMenu;
  }

  /**
  * Set le titreMenu de Page
  * @param  ?string $titreMenu
  * @return self
  */
  public function setTitreMenu(?string $titreMenu): self
  {
    $this->titreMenu = $titreMenu;

    return $this;
  }
}
