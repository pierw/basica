<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use App\Entity\PageTranslation;

/**
* @ORM\Entity(repositoryClass="App\Repository\PageRepository")
*/
class Page
{   use ORMBehaviors\Translatable\Translatable;


  public function __call($method, $arguments)
  {
    $method = ('get' === substr($method, 0, 3) || 'set' === substr($method, 0, 3)) ? $method : 'get'. ucfirst($method);

    return $this->proxyCurrentLocaleTranslation($method, $arguments);
  }

  public function __get($name)
  {
    $method = 'get'. ucfirst($name);
    $arguments = [];
    return $this->proxyCurrentLocaleTranslation($method, $arguments);
  }



  /**
  * id de Page
  * @var int
  * @ORM\Id()
  * @ORM\GeneratedValue()
  * @ORM\Column(type="integer")
  */
  private $id;


  /**
  * tri de Page
  * @var int
  * @ORM\Column(type="integer")
  */
  private $tri;

  /**
  * Récupère l'id de Page
  * @return int
  */
  public function getId(): ?int
  {
    return $this->id;
  }
  
  /**
  * Récupère tri de Page
  * @return int
  */
  public function getTri(): ?int
  {
    return $this->tri;
  }

  /**
  * Set tri de Page
  * @param  int  $tri
  * @return self
  */
  public function setTri(int $tri): self
  {
    $this->tri = $tri;

    return $this;
  }
}
