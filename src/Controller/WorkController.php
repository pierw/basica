<?php
/*
./src/Controller/WorkController.php

* Controller des Work
*
* Action disponible show
*
* @author Original Author Pierre Wasilewski
* @copyright 1997-2005 The PHP Group
* @version 1.0.1
*/

namespace App\Controller;
use Ieps\Core\GenericController;
use App\Entity\Work;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Knp\Component\Pager\PaginatorInterface;


/**
 * Controller des Work
 */

class WorkController extends GenericController {
      /**
       * Function qui va chercher les posts similaire du Tag x
       * @param  string $vue
       * @param  int $id
       * @return Response
       */
      public function similarAction(string $vue, $id){

          $tags = $this->_repository->findTag($id);

          $work = $this->_repository->findSimilar($tags,$id);



          return $this->render('work/'.$vue.'.html.twig',[
            // 'work'=>$work,
            'work'=>$work
      ]);

      }


      /**
       * Function qui crée la pagination des works par tag
       * @param  Request            $request
       * @param  string             $vue
       * @param  PaginatorInterface $paginator
       * @param  array              $orderBy
       * @param  int                $limit
       * @param  int                $work
       * @param  int                $tag
       * @return Response
       */

      public function paginationAction(Request $request,string $vue,PaginatorInterface $paginator  ,array $orderBy = ['dateCreation' => 'DESC'] ,int $limit = null,int $work= null,int $tag = null){
            $where =($work !== null)?['work'=>$work]:[];


            $request = Request::createFromGlobals();
            if($tag !== null) {
            $paginate = $this->_repository->findWorkByTag($tag);
            }
            else {
            $paginate = $this->_repository->findBy($where, $orderBy, $limit);
            }
            $work= $paginator->paginate(
            $paginate, /* query NOT result */
            $request->query->getInt('page',1), /*page number*/
            $request->query->getInt('limit',3)
    );
            return $this->render('work/'.$vue.'.html.twig',[
            'work' => $work

          ]);
        }


      /**
       * Function pour charger plus de work on.click (AJAX)
       * @param  Request $request
       * @param  array   $orderBy
       * @param  integer $limit
       * @param  int     $work
       * @return Response
       */
      public function ajaxMoreAction(Request $request,array $orderBy = ['dateCreation' => 'DESC'] ,int $limit = 3,int $work = null){
        $where =($work !== null)?['work'=>$work]:[];
        if ($request->isXmlHttpRequest()) {
          $offset = $request->query->get('offset');
          $work = $this->_repository->findBy($where, $orderBy, $limit, $offset);
        return $this->render('work/liste.html.twig',[
          'work'  => $work
      ]);

       }
        return new Response("test ko", 400);
      }

 }
