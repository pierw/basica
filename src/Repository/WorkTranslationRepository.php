<?php

namespace App\Repository;

use App\Entity\WorkTranslation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
* @method WorkTranslation|null find($id, $lockMode = null, $lockVersion = null)
* @method WorkTranslation|null findOneBy(array $criteria, array $orderBy = null)
* @method WorkTranslation[]    findAll()
* @method WorkTranslation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
*/
class WorkTranslationRepository extends ServiceEntityRepository
{
  public function __construct(RegistryInterface $registry)
  {
    parent::__construct($registry, WorkTranslation::class);
  }

  // /**
  //  * @return WorkTranslation[] Returns an array of WorkTranslation objects
  //  */
  /*
  public function findByExampleField($value)
  {
  return $this->createQueryBuilder('w')
  ->andWhere('w.exampleField = :val')
  ->setParameter('val', $value)
  ->orderBy('w.id', 'ASC')
  ->setMaxResults(10)
  ->getQuery()
  ->getResult()
  ;
}
*/

/*
public function findOneBySomeField($value): ?WorkTranslation
{
return $this->createQueryBuilder('w')
->andWhere('w.exampleField = :val')
->setParameter('val', $value)
->getQuery()
->getOneOrNullResult()
;
}
*/
}
