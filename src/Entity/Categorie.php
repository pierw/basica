<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;


/**
* @ORM\Entity(repositoryClass="App\Repository\CategorieRepository")
*/
class Categorie
{
  use ORMBehaviors\Translatable\Translatable;


  /**
  * Function qui permet à easyAdmin d'avoir accès au traduction
  * @param  [type] $method
  * @param  [type] $arguments
  * @return [type]
  */
  public function __call($method, $arguments)
  {
    $method = ('get' === substr($method, 0, 3) || 'set' === substr($method, 0, 3)) ? $method : 'get'. ucfirst($method);

    return $this->proxyCurrentLocaleTranslation($method, $arguments);
  }
  /**
  * Function qui permet à easyAdmin d'avoir accès au traduction
  * @param  [type] $name
  * @return [type]
  */
  public function __get($name)
  {
    $method = 'get'. ucfirst($name);
    $arguments = [];
    return $this->proxyCurrentLocaleTranslation($method, $arguments);
  }
  /**
  * Categorie Id
  * @var int
  * @ORM\Id()
  * @ORM\GeneratedValue()
  * @ORM\Column(type="integer")
  */

  private $id;

  /**
  * posts POST
  * @var Post
  * @ORM\ManyToMany(targetEntity="App\Entity\Post", mappedBy="categorie")
  */

  private $posts;


  /**
  * function qui permet l'affichage des relation n m  dans l'easy admin
  * @return string [description]
  */
  public function __toString() {
    return $this->nom;
  }
  /**
  *  constructor
  *
  */
  public function __construct()
  {
    $this->posts = new ArrayCollection();
  }
  /**
  * Récupère l'id de Categorie
  * @return int
  */
  public function getId(): ?int
  {
    return $this->id;
  }

  /**
  * @return Collection|Post[]
  */
  public function getPosts(): Collection
  {
    return $this->posts;
  }
  /**
  * Ajout d'un Post
  * @param  Post $post
  * @return self
  */
  public function addPost(Post $post): self
  {
    if (!$this->posts->contains($post)) {
      $this->posts[] = $post;
      $post->addCategorie($this);
    }

    return $this;
  }
  /**
  * Delete d'un post
  * @param  Post $post
  * @return self
  */
  public function removePost(Post $post): self
  {
    if ($this->posts->contains($post)) {
      $this->posts->removeElement($post);
      $post->removeCategorie($this);
    }

    return $this;
  }
}
