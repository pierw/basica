<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Cocur\Slugify\Slugify;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PostTranslationRepository")
 */
class PostTranslation
{
  use ORMBehaviors\Translatable\Translation;


    /**
      * titre de Post(Translation)
      * @var string
      * @ORM\Column(type="string", length=255)
      */
    private $titre;


    /**
     * texteLead de Post(Translation)
     * @var string
     * @ORM\Column(type="text")
     */
    private $texteLead;

    /**
     * texteSuite de Post(Translation)
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    private $texteSuite;


    /**
     * Récupère le titre de Post
     * @return string
     */
    public function getTitre(): ?string
    {
        return $this->titre;
    }


    /**
    * Function qui permet de slugify le titre de Page
    * @return string
    */
    public function getSlug(){
      $slugify = new Slugify();
      return $slugify->slugify($this->titre);
    }

    /**
     * Set le titre de Post
     * @param  string $titre
     * @return self
     */
    public function setTitre(string $titre): self
    {
        $this->titre = $titre;

        return $this;
    }

    /**
      * Récupère le texteLead de Post
      * @return string
     */
    public function getTexteLead(): ?string
    {
        return $this->texteLead;
    }

    /**
     * Set le texteLead de Post
     * @param  string $texteLead
     * @return self
     */
    public function setTexteLead(string $texteLead): self
    {
        $this->texteLead = $texteLead;

        return $this;
    }

    /**
     * Récupère le texteSuite de Post
     * @return string
     */
    public function getTexteSuite(): ?string
    {
        return $this->texteSuite;
    }

    /**
     * Set le $texteSuite de Post
     * @param  ?string $texteSuite
     * @return self
     */
    public function setTexteSuite(?string $texteSuite): self
    {
        $this->texteSuite = $texteSuite;

        return $this;
    }
}
