<?php
/*
./src/Controller/PageController.php

* Controller des Page
*
* Action disponible show
*
* @author Original Author Pierre Wasilewski
* @copyright 1997-2005 The PHP Group
* @version 1.0.1
*/

namespace App\Controller;
use Ieps\Core\GenericController;
use App\Entity\Page;
use Symfony\Component\HttpFoundation\Request;
use MKebza\GoogleMaps\Service\StaticMap;


/**
 * Controller des Page
 */

class PageController extends GenericController {



    /**
     * Function qui permet de récupèrer le flux rss d'une page public facebook
     * @param  Request $request
     * @return Response
     */
    public function viewRSSAction(Request $request){

      $rss = simplexml_load_file('http://fetchrss.com/rss/5cb03ba08a93f8a73c8b45685cb03b778a93f8443c8b4567.xml');
      $arr = json_decode(json_encode($rss));

      return $this->render('page/my_rss.html.twig', array(
          'arr' => $arr,
        ));
      }

      /**
       * Function qui permet d'afficher le menu des pages
       * + highlight
       * @param  array $where
       * @param  int $limit
       * @param  int $id
       * @param  array $route
       * @return Response
       */
    public function indexPageAction($where=null,$limit=null,$id = null,$route){

          $pages = $this->_repository->findBy(
                                    [],
                                    ['tri' => 'asc'],
                                    $limit
                                  );
          return $this->render('page/index.html.twig',[
          'pages' => $pages,
          'id'=>$id,
          'route'=>$route
        ]);
      }

}
