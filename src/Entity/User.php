<?php
// src/Entity/User.php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
* @ORM\Entity
* @ORM\Table(name="fos_user")
*/
class User extends BaseUser
{
  /**
  * id de User
  * @var int
  * @ORM\Id
  * @ORM\Column(type="integer")
  * @ORM\GeneratedValue(strategy="AUTO")
  */
  protected $id;

  /**
  * Commentaire de User
  * @var Commentaire
  * @ORM\OneToMany(targetEntity="App\Entity\Commentaire", mappedBy="user")
  */
  private $commentaires;

  /**
  * Constructor de User
  */
  public function __construct()
  {
    parent::__construct();
    $this->commentaires = new ArrayCollection();
  }

  /**
  * @return Collection|Commentaire[]
  */
  /**
  * Récupère les Commentaire de User
  * @return Collection
  */
  public function getCommentaires(): Collection
  {
    return $this->commentaires;
  }
  /**
  * Set les commentaire de User
  * @param  Commentaire $commentaire
  * @return self
  */
  public function addCommentaire(Commentaire $commentaire): self
  {
    if (!$this->commentaires->contains($commentaire)) {
      $this->commentaires[] = $commentaire;
      $commentaire->setUser($this);
    }

    return $this;
  }

  /**
  * Delete des Commentaire de User
  * @param  Commentaire $commentaire [description]
  * @return self                     [description]
  */
  public function removeCommentaire(Commentaire $commentaire): self
  {
    if ($this->commentaires->contains($commentaire)) {
      $this->commentaires->removeElement($commentaire);
      // set the owning side to null (unless already changed)
      if ($commentaire->getUser() === $this) {
        $commentaire->setUser(null);
      }
    }

    return $this;
  }

  /**
  * Récupère l'id de User 
  * @return [type] [description]
  */
  public function getId(): ?int
  {
    return $this->id;
  }
}
