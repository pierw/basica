/*
* Welcome to your app's main JavaScript file!
*
* We recommend including the built version of this JavaScript file
* (and its CSS file) in your base layout (base.html.twig).
*/



// any CSS you require will output into a single css file (app.css in this case)
require('../css/app.css');
require('../css/bootstrap.css');
require('../css/bootstrap-responsive.min.css');
require('../css/custom.css');
require('../css/font-awesome.min.css');
require('../css/icomoon-social.css');
require('../css/main.css');



// Need jQuery? Install it with "yarn add jquery", then uncomment to require it.
// const $ = require('jquery');
const $ = require('jquery');

$(function(){

// je vais chercher le path et le nombre de posts déjà présent de la page quand je click sur moreBtn
  $('#moreBtn').on('click',function(e){
    e.preventDefault();
    var path = $("#moreBtn").attr("data-path");
    var nbrePosts = $('.js-count').length;
// je lance ma requête AJAX avec en offset le nobrePosts
    $.ajax({
      url: path,
      method: 'get',
      data: {
        offset:nbrePosts
      },
// en cas de success j'append la vue en dessous des posts déjà présent
      success: function(reponsePHP){
        $('#listePosts').append(reponsePHP)
        .find('.js-count:nth-last-of-type(-n+3)')
        .hide().slideDown();
      },
// message d'erreur en cas d'error
      error: function(){
        alert("Problème durant la transaction...");
      }
    });


  })
})
