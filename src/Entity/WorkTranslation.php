<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use App\Entity\Tag;
use App\Entity\WorkTranslation;
use Cocur\Slugify\Slugify;


/**
* @ORM\Entity(repositoryClass="App\Repository\WorkTranslationRepository")
*/
class WorkTranslation
{

  use ORMBehaviors\Translatable\Translation;



  /**
  * titre de Work(Translation)
  * @var string
  * @ORM\Column(type="string", length=255)
  */

  private $titre;

  /**
  * sousTitre de Work(Translation)
  * @var string
  * @ORM\Column(type="string", length=255)
  */

  private $sousTitre;

  /**
  * sousSousTitre de Work(Translation)
  * @var string
  * @ORM\Column(type="string", length=255, nullable=true)
  */

  private $sousSousTitre;

  /**
  * texte de Work(Translation)
  * @var string
  * @ORM\Column(type="text")
  */

  private $texte;


  /**
  * Constructor de WorkTranslation
  */
  public function __construct()
  {
    $this->works = new ArrayCollection();
  }

  /**
  * function qui permet d'afficher les liaisons n m dans easyAdmin
  * @return string
  */
  public function __toString() {

    return  $this->titre;

  }

  /**
  * Function qui permet de slugify le titre de Page
  * @return string
  */
  public function getSlug(){
    $slugify = new Slugify();
    return $slugify->slugify($this->titre);
  }

  /**
  * Récupère l'id de WorkTranslation
  * @return int
  */
  public function getId(): ?int
  {
    return $this->id;
  }

  /**
  * Récupère le titre de Work(Translation)
  * @return string
  */
  public function getTitre(): ?string
  {
    return $this->titre;
  }

  /**
  * Set le titre de Work(Translation)
  * @param  string $titre
  * @return self
  */
  public function setTitre(string $titre): self
  {
    $this->titre = $titre;

    return $this;
  }

  /**
  * Récupère le SousTitre de Work(Translation)
  * @return string
  */
  public function getSousTitre(): ?string
  {
    return $this->sousTitre;
  }

  /**
  * Set SousTitre de Work(translation)
  * @param  string $sousTitre
  * @return self
  */
  public function setSousTitre(string $sousTitre): self
  {
    $this->sousTitre = $sousTitre;

    return $this;
  }

  /**
  * Récupère sousSousTitre de Work(translation)
  * @return string
  */
  public function getSousSousTitre(): ?string
  {
    return $this->sousSousTitre;
  }

  /**
  * Set sousSousTitre de Work(translation)
  * @param  ?string $sousSousTitre
  * @return self
  */
  public function setSousSousTitre(?string $sousSousTitre): self
  {
    $this->sousSousTitre = $sousSousTitre;

    return $this;
  }

  /**
  * Récupère texte de Work(translation)
  * @return [type] [description]
  */
  public function getTexte(): ?string
  {
    return $this->texte;
  }

  /**
  * Set texte de Work(translation)
  * @param  string $texte
  * @return self
  */
  public function setTexte(string $texte): self
  {
    $this->texte = $texte;

    return $this;
  }


}
