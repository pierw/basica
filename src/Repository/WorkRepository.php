<?php

namespace App\Repository;

use App\Entity\Work;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
* @method Work|null find($id, $lockMode = null, $lockVersion = null)
* @method Work|null findOneBy(array $criteria, array $orderBy = null)
* @method Work[]    findAll()
* @method Work[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
*/
class WorkRepository extends ServiceEntityRepository
{
  public function __construct(RegistryInterface $registry)
  {
    parent::__construct($registry, Work::class);
  }

  // /**
  //  * @return Work[] Returns an array of Work objects
  //  */
  /*
  public function findByExampleField($value)
  {
  return $this->createQueryBuilder('w')
  ->andWhere('w.exampleField = :val')
  ->setParameter('val', $value)
  ->orderBy('w.id', 'ASC')
  ->setMaxResults(10)
  ->getQuery()
  ->getResult()
  ;
}
*/

/*
public function findOneBySomeField($value): ?Work
{
return $this->createQueryBuilder('w')
->andWhere('w.exampleField = :val')
->setParameter('val', $value)
->getQuery()
->getOneOrNullResult()
;
}
*/


/**
 * Function qui cherche les works ayant le tag x
 * @param  int  $id id du tag
 * @return Work
 */
public function findWorkByTag($id){

  return $this->createQueryBuilder('w')
  ->join('w.tag','t')
  ->where('t.id = :val')
  ->setParameter('val', $id)
  ->getQuery()
  ->getResult()
  ;
}






// je prend les nom des tags du work x id
public function findTag(int $id){
  return $this->createQueryBuilder('w')
  ->join('w.tag','t')
  ->select('t.nom')
  ->where('w.id = :val')
  ->setParameter('val', $id)
  ->getQuery()
  ->getResult()
  ;
}

// je count les works qui ont les même nom de tag
public function findSimilar(array $tags,int $id) {

  $qb = $this->createQueryBuilder('w');
  $qb->Innerjoin('w.tag', 't');
  $qb->addSelect('COUNT(w.id) as HIDDEN count');
  $qb->groupBy('w.id');
  foreach ($tags as $i => $tag) {
    $qb->Orwhere($qb->expr()->orX(
      $qb->expr()->like('t.nom', ":val".$i)
    ));
    $qb->setParameter('val'.$i, '%'.$tag["nom"].'%');
  }
  $qb->setParameter('id', $id);
  $qb->andWhere($qb->expr()->notIn('w.id', ":id"));
  $qb->orderBy('count', 'DESC');
  return  $qb->getQuery()->getResult();
  ;
}




}
