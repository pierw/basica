<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use App\Entity\Work;


/**
* @ORM\Entity(repositoryClass="App\Repository\WorkRepository")
* @Vich\Uploadable
*/
class Work
{
  use ORMBehaviors\Translatable\Translatable;


  public function __get($name)
  {
    $method = 'get'. ucfirst($name);
    $arguments = [];
    return $this->proxyCurrentLocaleTranslation($method, $arguments);
  }


  public function __call($method, $arguments)
  {
    $method = ('get' === substr($method, 0, 3) || 'set' === substr($method, 0, 3)) ? $method : 'get'. ucfirst($method);

    return $this->proxyCurrentLocaleTranslation($method, $arguments);
  }

  
  /**
  * id de Work
  * @var int
  * @ORM\Id()
  * @ORM\GeneratedValue()
  * @ORM\Column(type="integer")
  */

  private $id;

  /**
  * dateCreation de Work
  * @var [type]
  * @ORM\Column(type="datetime")
  */

  private $dateCreation;

  /**
  * image de Work
  * @var datetime
  * @ORM\Column(type="string", length=255, nullable=true)
  */

  private $image;



  /**
  * Client de Work
  * @var Client
  * @ORM\ManyToOne(targetEntity="App\Entity\Client", inversedBy="works")
  */

  private $client;

  /**
  * enAvant de Work
  * @var int
  * @ORM\Column(type="integer")
  */

  private $enAvant;

  /**
  * dateModification de Work
  * @var datetime
  * @ORM\Column(type="datetime", nullable=true)
  */

  private $dateModification;

  /**
  * imageFile de Work
  * @var File
  * @Vich\UploadableField(mapping="uploads_portfolio", fileNameProperty="image")
  * @var File
  */

  private $imageFile;

  /**
  * Tag de Work
  * @var Tag
  * @ORM\ManyToMany(targetEntity="App\Entity\Tag", inversedBy="works")
  */

  private $tag;



  /**
  * Function qui permet d'afficher les liaisons n m dans easy admin
  * @return string [description]
  */
  public function __toString() {
    return $this->titre;
  }

  /**
  * Constructor de Work
  */
  public function __construct()
  {
    $this->setDateCreation(new \DateTime());
    $this->tag = new ArrayCollection();
  }

  /**
  * Récupère l'id de work
  * @return int
  */
  public function getId(): ?int
  {
    return $this->id;
  }

  /**
  * Récupère la dateCreation de Work
  * @return datetime
  */
  public function getDateCreation(): ?\DateTimeInterface
  {
    return $this->dateCreation;
  }
  /**
  * Set la dateCreation de Work
  * @param  DateTimeInterface $dateCreation
  * @return self
  */
  public function setDateCreation(\DateTimeInterface $dateCreation): self
  {
    $this->dateCreation = $dateCreation;

    return $this;
  }

  /**
  * Récupère l'image de Work
  * @return string
  */
  public function getImage(): ?string
  {
    return $this->image;
  }

  /**
  * Set l'image de Work
  * @param  ?string $image
  * @return self
  */
  public function setImage(?string $image): self
  {
    $this->image = $image;

    return $this;
  }

  /**
  * Set l'imageFile de Work
  * @param File $image
  */
  public function setImageFile(File $image = null)
  {
    $this->imageFile = $image;

    if ($image) {
      $this->dateModification = new \DateTime('now');
    }
  }

  /**
  * Récupère l'imageFile de Work
  * @return File
  */
  public function getImageFile()
  {
    return $this->imageFile;
  }


  /**
  * Récupère les client de Work
  * @return Client
  */
  public function getClient(): ?Client
  {
    return $this->client;
  }

  /**
  * Set les Client de Work
  * @param  ?Client $client
  * @return self
  */
  public function setClient(?Client $client): self
  {
    $this->client = $client;

    return $this;
  }

  /**
  * Récupère enAvant de Work
  * @return int
  */
  public function getEnAvant(): ?int
  {
    return $this->enAvant;
  }

  /**
  * Set $enAvant de Work
  * @param  int  $enAvant
  * @return self
  */
  public function setEnAvant(int $enAvant): self
  {
    $this->enAvant = $enAvant;

    return $this;
  }

  /**
  * Récupère dateModification de Work
  * @return DateTime
  */
  public function getDateModification(): ?\DateTimeInterface
  {
    return $this->dateModification;
  }

  /**
  * Set dateModification de Work
  * @param  DateTimeInterface $dateModification [description]
  * @return self                                [description]
  */
  public function setDateModification(\DateTimeInterface $dateModification): self
  {
    $this->dateModification = $dateModification;

    return $this;
  }

  /**
  * @return Collection|Tag[]
  */
  /**
  * Récupère Tag de Work
  * @return Collection
  */
  public function getTag(): Collection
  {
    return $this->tag;
  }

  /**
  * Ajoute Tag dans Work
  * @param  Tag  $tag
  * @return self
  */
  public function addTag(Tag $tag): self
  {
    if (!$this->tag->contains($tag)) {
      $this->tag[] = $tag;
    }

    return $this;
  }

  /**
  * Delete Tag dans Work
  * @param  Tag  $tag
  * @return self
  */
  public function removeTag(Tag $tag): self
  {
    if ($this->tag->contains($tag)) {
      $this->tag->removeElement($tag);
    }

    return $this;
  }




}
