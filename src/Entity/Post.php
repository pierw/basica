<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
* @ORM\Entity(repositoryClass="App\Repository\PostRepository")
* @Vich\Uploadable
*/
class Post
{
  use ORMBehaviors\Translatable\Translatable;


  public function __call($method, $arguments)
  {
    $method = ('get' === substr($method, 0, 3) || 'set' === substr($method, 0, 3)) ? $method : 'get'. ucfirst($method);

    return $this->proxyCurrentLocaleTranslation($method, $arguments);
  }

  public function __get($name)
  {
    $method = 'get'. ucfirst($name);
    $arguments = [];
    return $this->proxyCurrentLocaleTranslation($method, $arguments);
  }

  /**
  * Constructor de Post
  */
  public function __construct()
  {
    $this->setDateCreation(new \DateTime());
    $this->categorie = new ArrayCollection();
    $this->commentaire = new ArrayCollection();

  }
  /**
  * id de Post
  * @var int
  * @ORM\Id()
  * @ORM\GeneratedValue()
  * @ORM\Column(type="integer")
  */
  private $id;


  /**
  * dateCreation de Post
  * @var datetime
  * @ORM\Column(type="datetime")
  */

  private $dateCreation;

  /**
  * image de Post
  * @var string
  * @ORM\Column(type="string", length=255, nullable=true)
  */
  private $image;



  /**
  * commentaire de Post
  * @var Commentaire
  * @ORM\OneToMany(targetEntity="App\Entity\Commentaire", mappedBy="post")
  */
  private $commentaire;


  /**
  * dateModification de Post
  * @var datetime
  * @ORM\Column(type="datetime",nullable=true)
  */
  private $dateModification;

  /**
  * imageFile de Post
  * @var File
  * @Vich\UploadableField(mapping="uploads_blog", fileNameProperty="image")
  * @var File
  */
  private $imageFile;


  /**
  * categorie de Post
  * @var Categorie
  * @ORM\ManyToMany(targetEntity="App\Entity\Categorie", inversedBy="posts")
  */
  private $categorie;



  /**
  * Récupère l'id de Post
  * @return int
  */
  public function getId(): ?int
  {
    return $this->id;
  }

  /**
  * Récupère la dateCreation de Post
  * @return datetime
  */
  public function getDateCreation(): ?\DateTimeInterface
  {
    return $this->dateCreation;
  }
  /**
  * Set dateCreation de Post
  * @param  DateTimeInterface $dateCreation
  * @return self
  */
  public function setDateCreation(\DateTimeInterface $dateCreation): self
  {
    $this->dateCreation = $dateCreation;

    return $this;
  }
  /**
  * Récupère l'image de Post
  * @return string
  */
  public function getImage(): ?string
  {
    return $this->image;
  }

  /**
  * Set l'image de Post
  * @param  ?string $image
  * @return self
  */
  public function setImage(?string $image): self
  {
    $this->image = $image;

    return $this;
  }

  /**
  * Set l'imageFile de Post
  * @param string $image
  */
  public function setImageFile(File $image = null)
  {
    $this->imageFile = $image;
    if ($image) {
      $this->dateModification = new \DateTime('now');
    }
  }

  /**
  * Set l'imageFile de Post
  * @return File
  */
  public function getImageFile()
  {
    return $this->imageFile;
  }



  /**
  * @return Collection|Commentaire[]
  */
  /**
  * Récupère les Commentaire de Post
  * @return Collection
  */
  public function getCommentaire(): Collection
  {
    return $this->commentaire;
  }
  /**
  * Ajoute les Commentaire à Post
  * @param  Commentaire $commentaire
  * @return self
  */
  public function addCommentaire(Commentaire $commentaire): self
  {
    if (!$this->commentaire->contains($commentaire)) {
      $this->commentaire[] = $commentaire;
      $commentaire->setPost($this);
    }

    return $this;
  }

  /**
  * Delete les Commentaire de Post
  * @param  Commentaire $commentaire
  * @return self
  */
  public function removeCommentaire(Commentaire $commentaire): self
  {
    if ($this->commentaire->contains($commentaire)) {
      $this->commentaire->removeElement($commentaire);
      // set the owning side to null (unless already changed)
      if ($commentaire->getPost() === $this) {
        $commentaire->setPost(null);
      }
    }
    return $this;
  }

  /**
  * Récupère la dateModification de Post
  * @return datetime
  */
  public function getDateModification(): ?\DateTimeInterface
  {
    return $this->dateModification;
  }

  /**
  * Set la dateModification de Post
  * @param  DateTimeInterface $dateModification
  * @return self
  */
  public function setDateModification(\DateTimeInterface $dateModification): self
  {
    $this->dateModification = $dateModification;

    return $this;
  }

  /**
  * @return Collection|Categorie[]
  */
  /**
  * Récupère la categorie de Post
  * @return Collection
  */
  public function getCategorie(): Collection
  {

    return $this->categorie;
  }

  /**
  * Ajoute des Categorie à Post
  * @param  Categorie $categorie
  * @return self
  */
  public function addCategorie(Categorie $categorie): self
  {
    if (!$this->categorie->contains($categorie)) {
      $this->categorie[] = $categorie;
    }

    return $this;
  }

  /**
  * Delete des Categorie à Post
  * @param  Categorie $categorie
  * @return self
  */
  public function removeCategorie(Categorie $categorie): self
  {
    if ($this->categorie->contains($categorie)) {
      $this->categorie->removeElement($categorie);
    }

    return $this;
  }



}
